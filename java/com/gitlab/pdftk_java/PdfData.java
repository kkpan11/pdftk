/*
 *   This file is part of the pdftk port to java
 *
 *   Copyright (c) Marc Vinyals 2017-2023
 *
 *   The program is a java port of PDFtk, the PDF Toolkit
 *   Copyright (c) 2003-2013 Steward and Lee, LLC
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   The program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.pdftk_java;

import java.util.ArrayList;

class PdfData {
  ArrayList<PdfInfo> m_info = new ArrayList<PdfInfo>();
  ArrayList<PdfBookmark> m_bookmarks = new ArrayList<PdfBookmark>();
  ArrayList<PdfPageLabel> m_pagelabels = new ArrayList<PdfPageLabel>();
  ArrayList<PdfPageMedia> m_pagemedia = new ArrayList<PdfPageMedia>();

  static final String PREFIX = "PdfID";
  static final String ID_0_LABEL = "PdfID0:";
  static final String ID_1_LABEL = "PdfID1:";
  static final String NUM_PAGES_LABEL = "NumberOfPages:";

  int m_num_pages = -1;

  String m_id_0 = null;
  String m_id_1 = null;

  public String toString() {
    StringBuilder ss = new StringBuilder();
    for (PdfInfo vit : m_info) {
      ss.append(vit);
    }
    ss.append(ID_0_LABEL + " " + m_id_0 + System.lineSeparator());
    ss.append(ID_1_LABEL + " " + m_id_1 + System.lineSeparator());
    ss.append(NUM_PAGES_LABEL + " " + m_num_pages + System.lineSeparator());
    for (PdfBookmark vit : m_bookmarks) {
      ss.append(vit);
    }
    return ss.toString();
  }

  boolean loadNumPages(String buff) {
    LoadableInt loader = new LoadableInt(m_num_pages);
    boolean success = loader.LoadInt(buff, NUM_PAGES_LABEL);
    m_num_pages = loader.ii;
    return success;
  }

  boolean loadID0(String buff) {
    LoadableString loader = new LoadableString(m_id_0);
    boolean success = loader.LoadString(buff, ID_0_LABEL);
    m_id_0 = loader.ss;
    return success;
  }

  boolean loadID1(String buff) {
    LoadableString loader = new LoadableString(m_id_1);
    boolean success = loader.LoadString(buff, ID_1_LABEL);
    m_id_1 = loader.ss;
    return success;
  }
}
